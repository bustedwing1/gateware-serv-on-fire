# ls -1 software/*.c |sed 's#.*software.\(.*\).c#gcc ./software/\1.c -o ./bin/\1#' |tee compile_c_programs.sh
mkdir -p ./bin
gcc ./software/adcfifo.c -o ./bin/adcfifo
gcc ./software/blinky2.c -o ./bin/blinky2
gcc ./software/blinky.c -o ./bin/blinky
gcc ./software/memread.c -o ./bin/memread
gcc ./software/memwrite.c -o ./bin/memwrite
gcc ./software/runserv.c -o ./bin/runserv
ls -l ./bin/*

