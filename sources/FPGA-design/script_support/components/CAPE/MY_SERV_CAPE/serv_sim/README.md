
Running Ubuntu 22.04
--------------------

```bash
sudo apt update
sudo apt upgrade
sudo apt install iverilog gtkwaves
```

Simulate SERV RISC-V Processor
------------------------------

```bash
./scripts/compile_iverilog.sh 
./scripts/sim_iverilog.sh 
gtkwaves test.vcd
```


  

